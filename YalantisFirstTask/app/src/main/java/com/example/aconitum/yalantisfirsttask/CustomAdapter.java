package com.example.aconitum.yalantisfirsttask;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.ViewHolder> {

    private ArrayList<MyImages> mDataset;
    public Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public ImageView Image1;
        public ImageView Image2;
        public ViewHolder(View itemView) {
            super(itemView);
            this.Image1 = (ImageView)itemView.findViewById(R.id.my_img1);
            this.Image2 = (ImageView)itemView.findViewById(R.id.my_img2);
        }
    }
    public CustomAdapter(Context context,ArrayList<MyImages> arrayList) {
        this.context = context;
        mDataset = arrayList;
    }
    @Override
    public CustomAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_item, parent, false);

        return new ViewHolder(view);

    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {


         final ImageView img1 = holder.Image1;
         final ImageView img2 = holder.Image2;

        Picasso.with(context)
                .load("http://cs11519.vk.me/u140111917/a_e489e2b6.jpg")
                .resize(90, 90)
        .into(img1);

        Picasso.with(context)
                .load("http://cs5778.userapi.com/u7078398/-6/x_4a4fcd93.jpg")
                .resize(90,90)
                .into(img2);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
