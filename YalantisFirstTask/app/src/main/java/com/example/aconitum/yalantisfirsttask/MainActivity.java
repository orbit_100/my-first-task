package com.example.aconitum.yalantisfirsttask;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.Menu;
import android.view.View;
import android.view.MotionEvent;

import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    public ArrayList<MyImages> images;

    final String ATTRIBUTE_NAME_TEXT = "text";
    final String ATTRIBUTE_NAME_DATE = "date";

    String[] textStatus;
    String[] textDate;

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TextView title_tv = (TextView) findViewById(R.id.title);

        if (title_tv != null) {title_tv.setOnTouchListener(this);}

       TextView yellow_tv = (TextView) findViewById(R.id.yellow_text_view);

        if (yellow_tv != null) { yellow_tv.setOnTouchListener(this);}

       TextView bottom_tv = (TextView) findViewById(R.id.bottom_text_view);

        if (bottom_tv != null) {bottom_tv.setOnTouchListener(this);}

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        if (recyclerView != null) {recyclerView.setOnTouchListener(this);}

        listView = (ListView) findViewById(R.id.my_list_view);
        if (listView != null) {listView.setOnTouchListener(this);}

        if (recyclerView != null) {recyclerView.setHasFixedSize(true);}

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);

        if (recyclerView != null) {recyclerView.setLayoutManager(mLayoutManager);}

        fillingList();

        RecyclerView.Adapter mAdapter = new CustomAdapter(MainActivity.this, images);

        if (recyclerView != null) {recyclerView.setAdapter(mAdapter);}

        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {

            actionBar.setDisplayHomeAsUpEnabled(true);

            actionBar.setDisplayShowHomeEnabled(true);
        }
        textStatus = getResources().getStringArray(R.array.text_status);
        textDate = getResources().getStringArray(R.array.date_status);

        ArrayList<Map<String, Object>> data = new ArrayList<>(
                textStatus.length);
        Map<String, Object> m;
        for (int i = 0; i < textStatus.length; i++) {
            m = new HashMap<>();
            m.put(ATTRIBUTE_NAME_TEXT, textStatus[i]);
            m.put(ATTRIBUTE_NAME_DATE, textDate[i]);
            data.add(m);
        }

        String[] from = {ATTRIBUTE_NAME_TEXT, ATTRIBUTE_NAME_DATE};

        int[] to = {R.id.tvText1, R.id.tvText2};

        SimpleAdapter sAdapter = new SimpleAdapter(this, data, R.layout.item,
                from, to);

        if (listView != null) {listView.setAdapter(sAdapter);}

    }

    public void fillingList() {

        images = new ArrayList<>();

        images.add(new MyImages("Tai Lung", "Usi"));

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        if(event.getAction()== MotionEvent.ACTION_DOWN) {

            switch (v.getId()) {

                case R.id.title:
                    Toast.makeText(this, "Title", Toast.LENGTH_LONG).show();
                    break;
                case R.id.yellow_text_view:
                    Toast.makeText(this, "Task status", Toast.LENGTH_LONG).show();
                    break;
                case R.id.my_list_view:
                    Toast.makeText(this, "List", Toast.LENGTH_LONG).show();
                    break;
                case R.id.bottom_text_view:
                    Toast.makeText(this, "Discription", Toast.LENGTH_LONG).show();
                    break;
                case R.id.my_recycler_view:
                    Toast.makeText(this, "Image", Toast.LENGTH_LONG).show();
                    break;
            }
        }
        return false;
    }

}